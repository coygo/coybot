// Copyright 2018 coy <https://gitlab.com/coygo>. All rights reserved.
// Use of this source code is governed by a GPLv3
// license that can be found in the LICENSE file.

package rssutil

import (
	"crypto/md5"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"time"
)

// RSS is a Web content syndication format.
//
// Its name is an acronym for Really Simple Syndication.
//
// RSS is a dialect of XML. All RSS files must conform to the XML 1.0
// specification, as published on the World Wide Web Consortium (W3C)
// website.
//
// A summary of RSS version history.
//
// At the top level, a RSS document is a <rss> element, with a mandatory
// attribute called version, that specifies the version of RSS that the
// document conforms to. If it conforms to this specification, the
// version attribute must be 2.0.
//
// Subordinate to the <rss> element is a single <channel> element, which
// contains information about the channel (metadata) and its contents.
type RSS struct {
	Version string     `xml:"version,attr" json:"version"`
	Channel RSSChannel `xml:"channel"      json:"channel"`
}

func (rss RSS) String() string {
	return fmt.Sprintf("RSS{Version:%q, Channel:%v}", rss.Version, rss.Channel)
}

type RSSChannel struct {

	/*************************** Required elements ***************************/

	// The name of the channel. It's how people refer to your service. If
	// you have an HTML website that contains the same information as
	// your RSS file, the title of your channel should be the same as the
	// title of your website.
	//
	// Sample:
	//   GoUpstate.com News Headlines
	Title string `xml:"title" json:"title"`

	// The URL to the HTML website corresponding to the channel.
	//
	// Sample:
	//   http://www.goupstate.com/
	Link string `xml:"link" json:"link"`

	// Phrase or sentence describing the channel.
	//
	// Sample:
	//   The latest news from GoUpstate.com, a Spartanburg Herald-Journal Web site.
	Description string `xml:"description" json:"description"`

	/*************************** Optional elements ***************************/

	// The language the channel is written in. This allows aggregators to
	// group all Italian language sites, for example, on a single page.
	// A list of allowable values for this element, as provided by
	// Netscape, is [here](https://cyber.harvard.edu/rss/languages.html).
	// You may also use [values defined](https://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes)
	// by the W3C.
	//
	// Sample:
	//   en-us
	Language string `xml:"language,omitempty" json:"language,omitempty"`

	// Copyright notice for content in the channel.
	//
	// Sample:
	//   Copyright 2002, Spartanburg Herald-Journal
	Copyright string `xml:"copyright,omitempty" json:"copyright,omitempty"`

	// Email address for person responsible for editorial content.
	//
	// Sample:
	//   geo@herald.com (George Matesky)
	ManagingEditor string `xml:"managingEditor,omitempty" json:"managing_editor,omitempty"`

	// Email address for person responsible for technical issues relating to channel.
	//
	// Sample:
	//   betty@herald.com (Betty Guernsey)
	WebMaster string `xml:"webMaster,omitempty" json:"webmaster,omitempty"`

	// The publication date for the content in the channel. For example,
	// the New York Times publishes on a daily basis, the publication
	// date flips once every 24 hours. That's when the pubDate of the
	// channel changes. All date-times in RSS conform to the Date and
	// Time Specification of [RFC 822](http://asg.web.cmu.edu/rfc/rfc822.html),
	// with the exception that the year may be expressed with two
	// characters or four characters (four preferred).
	//
	// Sample:
	//   Sat, 07 Sep 2002 00:00:01 GMT
	PubDate *RFC822 `xml:"pubDate,omitempty" json:"pub_date,omitempty"`

	// The last time the content of the channel changed.
	//
	// Sample:
	//   Sat, 07 Sep 2002 09:42:31 GMT
	LastBuildDate *RFC822 `xml:"lastBuildDate,omitempty" json:"last_build_date,omitempty"`

	// Specify one or more categories that the channel belongs to.
	// Follows the same rules as the <item>-level
	// [category](https://cyber.harvard.edu/rss/rss.html#ltcategorygtSubelementOfLtitemgt)
	// element. More [info](https://cyber.harvard.edu/rss/rss.html#syndic8).
	//
	// Sample:
	//   <category>Newspapers</category>
	Categories RSSCategories `xml:"category,omitempty" json:"category,omitempty"`

	// A string indicating the program used to generate the channel.
	//
	// Sample:
	//   MightyInHouse Content System v2.3
	Generator string `xml:"generator,omitempty" json:"generator,omitempty"`

	// A URL that points to the documentation for the format used in the
	// RSS file. It's probably a pointer to this page. It's for people
	// who might stumble across an RSS file on a Web server 25 years from
	// now and wonder what it is.
	//
	// Sample:
	//   http://blogs.law.harvard.edu/tech/rss
	Docs string `xml:"docs,omitempty" json:"docs,omitempty"`

	// Allows processes to register with a cloud to be notified of
	// updates to the channel, implementing a lightweight
	// publish-subscribe protocol for RSS feeds. More info
	// [here](https://cyber.harvard.edu/rss/rss.html#ltcloudgtSubelementOfLtchannelgt).
	//
	// Sample:
	//   <cloud domain="rpc.sys.com" port="80" path="/RPC2" registerProcedure="pingMe" protocol="soap"/>
	Cloud *RSSCloud `xml:"cloud,omitempty" json:"cloud,omitempty"`

	// TTL stands for time to live. It's a number of minutes that
	// indicates how long a channel can be cached before refreshing from
	// the source. More info [here](https://cyber.harvard.edu/rss/rss.html#ltttlgtSubelementOfLtchannelgt).
	//
	// Sample:
	//   <ttl>60</ttl>
	TTL int `xml:"ttl,omitempty" json:"ttl,omitempty"`

	// Specifies a GIF, JPEG or PNG image that can be displayed with the
	// channel.
	// More info [here](https://cyber.harvard.edu/rss/rss.html#ltimagegtSubelementOfLtchannelgt).
	Image *RSSImage `xml:"image,omitempty" json:"image,omitempty"`

	// The [PICS](https://www.w3.org/PICS/) rating for the channel.
	Rating string `xml:"rating,omitempty" json:"rating,omitempty"`

	// Specifies a text input box that can be displayed with the channel.
	// More info [here](https://cyber.harvard.edu/rss/rss.html#lttextinputgtSubelementOfLtchannelgt).
	TextInput *RSSTextInput `xml:"textInput,omitempty" json:"text_input,omitempty"`

	// A hint for aggregators telling them which hours they can skip.
	// More info [here](https://cyber.harvard.edu/rss/skipHoursDays.html#skiphours).
	SkipHours []int `xml:"skipHours>hour,omitempty" json:"skip_hours,omitempty"`

	// A hint for aggregators telling them which days they can skip.
	// More info [here](https://cyber.harvard.edu/rss/skipHoursDays.html#skipdays).
	SkipDays []time.Weekday `xml:"skipDays>day,omitempty" json:"skip_days,omitempty"`

	Items RSSItems `xml:"item,omitempty" json:"items,omitempty"`
}

func (c RSSChannel) String() string {
	return fmt.Sprintf(
		"RSSChannel{Title:%q, Link:%q, Description:%q, Language:%q, "+
			"Copyright:%q, ManagingEditor:%q, WebMaster:%q, PubDate:%v, "+
			"LastBuildDate:%v, Categories:%v, Generator:%q, Docs:%q, "+
			"Cloud:%v, TTL:%d, Image:%v, Rating:%q, TextInput:%v, "+
			"SkipHours:%v, SkipDays:%v, Items:%v}",
		c.Title, c.Link, c.Description, c.Language, c.Copyright,
		c.ManagingEditor, c.WebMaster, c.PubDate, c.LastBuildDate, c.Categories,
		c.Generator, c.Docs, c.Cloud, c.TTL, c.Image, c.Rating, c.TextInput,
		c.SkipHours, c.SkipDays, c.Items,
	)
}

type RSSCategories []RSSCategory

func (c RSSCategories) Equal(t RSSCategories) bool {
	if len(c) != len(t) {
		return false
	}

	v1 := make(map[string]string)
	v2 := make(map[string]string)

	for _, v := range c {
		v1[v.Category] = v.Domain
	}
	for _, v := range t {
		v2[v.Category] = v.Domain
	}

	// NOTE: stupid comperations, need to improve.
	for k := range v1 { // NOTE: for k, v : range v1 { ... }
		if v1[k] != v2[k] {
			return false
		}
	}
	for k := range v2 { // NOTE: for k, v : range v1 { ... }
		if v2[k] != v1[k] {
			return false
		}
	}

	return true
}

// RSSCategory is an optional sub-element of RSSChannel/RSSItem.
//
// It has one optional attribute, domain, a string that identifies a
// categorization taxonomy.
//
// The value of the element is a forward-slash-separated string that
// identifies a hierarchic location in the indicated taxonomy. Processors
// may establish conventions for the interpretation of categories. Two
// examples are provided below:
//
// <category>Grateful Dead</category>
//
// <category domain="http://www.fool.com/cusips">MSFT</category>
//
// You may include as many category elements as you need to, for
// different domains, and to have an item cross-referenced in different
// parts of the same domain.
type RSSCategory struct {

	/*************************** Required elements ***************************/

	Category string `xml:",chardata" json:"category"`

	/*************************** Optional elements ***************************/

	Domain string `xml:"domain,attr,omitempty" json:"domain,omitempty"`
}

func (c RSSCategory) String() string {
	return fmt.Sprintf(
		"RSSCategory{Category:%q, Domain:%q}",
		c.Category, c.Domain,
	)
}

func (c RSSCategory) Equal(t RSSCategory) bool {
	return c.Category == t.Category && c.Domain == t.Domain
}

// RSSCloud is an optional sub-element of RSSChannel. It specifies a web
// service that supports the RSSCloud interface which can be implemented
// in HTTP-POST, XML-RPC or SOAP 1.1.
//
// Its purpose is to allow processes to register with a cloud to be
// notified of updates to the channel, implementing a lightweight
// publish-subscribe protocol for RSS feeds.
//
// <cloud domain="rpc.sys.com" port="80" path="/RPC2" registerProcedure="myCloud.rssPleaseNotify" protocol="xml-rpc" />
//
// In this example, to request notification on the channel it appears in,
// you would send an XML-RPC message to rpc.sys.com on port 80, with a
// path of /RPC2. The procedure to call is myCloud.rssPleaseNotify.
//
// A full explanation of this element and the RSSCloud interface is
// [here](https://cyber.harvard.edu/rss/soapMeetsRss.html#rsscloudInterface).
type RSSCloud struct {

	/*************************** Required elements ***************************/

	Domain            string `xml:"domain,attr" json:"domain"`
	Port              int    `xml:"port,attr" json:"port"`
	Path              string `xml:"path,attr" json:"path"`
	RegisterProcedure string `xml:"registerProcedure,attr" json:"register_procedure"`
	Protocol          string `xml:"protocol,attr" json:"protocol"`

	/*************************** Optional elements ***************************/

	// No optional element.
}

func (c RSSCloud) String() string {
	return fmt.Sprintf(
		"RSSCloud{Domain:%q, Port:%d, Path:%q, RegisterProcedure:%q, "+
			"Protocol:%q}",
		c.Domain, c.Port, c.Path, c.RegisterProcedure, c.Protocol,
	)
}

func (c RSSCloud) Equal(t RSSCloud) bool {
	return c.Domain == t.Domain && c.Port == t.Port && c.Path == t.Path &&
		c.RegisterProcedure == t.RegisterProcedure && c.Protocol == t.Protocol
}

// RSSImage is an optional sub-element of RSSChannel, which contains
// three required and three optional sub-elements.
type RSSImage struct {

	/*************************** Required elements ***************************/

	// URL is the URL of a GIF, JPEG or PNG image that represents the
	// Channel.
	URL string `xml:"url" json:"url"`

	// Title describes the image, it's used in the ALT attribute of the
	// HTML <img> tag when the channel is rendered in HTML.
	Title string `xml:"title" json:"title"`

	// Link is the URL of the site, when the channel is rendered, the
	// image is a link to the site. (Note, in practice the image Title
	// and Link should have the same value as the Channel's Title and Link.
	Link string `xml:"link" json:"link"`

	/*************************** Optional elements ***************************/

	// Width is an optional elements, in numbers, indicating the width of
	// the image in pixels.
	//
	// Maximum value for width is 144, default value is 88.
	Width int `xml:"width,omitempty" json:"width,omitempty"`

	// Height is an optional elements, in numbers, indicating the height
	// of the image in pixels.
	//
	// Maximum value for height is 400, default value is 31.
	Height int `xml:"height,omitempty" json:"height,omitempty"`

	// Description is an optional elements, which contains text that is
	// included in the TITLE attribute of the link formed around the
	// image in the HTML rendering.
	Description string `xml:"description,omitempty" json:"description,omitempty"`
}

func (img RSSImage) String() string {
	return fmt.Sprintf(
		"RSSImage{URL:%q, Title:%q, Link:%q, Width:%d, Height:%d, "+
			"Description:%q}",
		img.URL, img.Title, img.Link, img.Width, img.Height, img.Description,
	)
}

func (img RSSImage) Equal(t RSSImage) bool {
	return img.URL == t.URL && img.Title == t.Title && img.Link == t.Link &&
		img.Width == t.Width && img.Height == t.Height &&
		img.Description == t.Description
}

// RSSTextInput is an optional sub-element of RSSChannel, which contains
// four required sub-elements.
//
// The purpose of the TextInput element is something of a mystery. You
// can use it to specify a search engine box. Or to allow a reader to
// provide feedback. Most aggregators ignore it.
type RSSTextInput struct {

	/*************************** Required elements ***************************/

	// The label of the Submit button in the text input area.
	Title string `xml:"title" json:"title"`

	// Explains the text input area.
	Description string `xml:"description" json:"description"`

	// The name of the text object in the text input area.
	Name string `xml:"name" json:"name"`

	// The URL of the CGI script that processes text input requests.
	Link string `xml:"link" json:"link"`

	/*************************** Optional elements ***************************/

	// No optional element.
}

func (ti RSSTextInput) String() string {
	return fmt.Sprintf(
		"RSSTextInput{Title:%q, Description:%q, Name:%q, Link:%q}",
		ti.Title, ti.Description, ti.Name, ti.Link,
	)
}

func (ti RSSTextInput) Equal(t RSSTextInput) bool {
	return ti.Title == t.Title && ti.Description == t.Description &&
		ti.Name == t.Name && ti.Link == t.Link
}

type RSSItems []RSSItem

func (it RSSItems) Equal(t RSSItems) bool {
	if len(it) != len(t) {
		return false
	}

	for i := 0; i < len(it); i++ {
		if !it[i].Equal(t[i]) {
			return false
		}
	}

	return true
}

// A channel may contain any number of <item>s. An item may represent a
// "story" -- much like a story in a newspaper or magazine; if so its
// description is a synopsis of the story, and the link points to the
// full story. An item may also be complete in itself, if so, the
// description contains the text (entity-encoded HTML is allowed; see
// [examples](https://cyber.harvard.edu/rss/encodingDescriptions.html)),
// and the link and title may be omitted. All elements of an item are
// optional, however at least one of title or description must be present.
type RSSItem struct {
	// The title of the item.
	//
	// Sample:
	//   Venice Film Festival Tries to Quit Sinking
	Title string `xml:"title,omitempty" json:"title,omitempty"`

	// The URL of the item.
	//
	// Sample:
	//   http://nytimes.com/2004/12/07FEST.html
	Link string `xml:"link,omitempty" json:"link,omitempty"`

	// The item synopsis.
	//
	// Sample:
	//   Some of the most heated chatter at the Venice Film Festival this
	//   week was about the way that the arrival of the stars at the
	//   Palazzo del Cinema was being staged.
	Description string `xml:"description,omitempty" json:"description,omitempty"`

	// Email address of the author of the item.
	// [More](https://cyber.harvard.edu/rss/rss.html#ltauthorgtSubelementOfLtitemgt).
	//
	// Sample:
	//   oprah@oxygen.net
	Author string `xml:"author,omitempty" json:"author,omitempty"`

	// Includes the item in one or more categories.
	// [More](https://cyber.harvard.edu/rss/rss.html#ltcategorygtSubelementOfLtitemgt).
	Categories RSSCategories `xml:"category,omitempty" json:"category,omitempty"`

	// URL of a page for comments relating to the item.
	// [More](https://cyber.harvard.edu/rss/rss.html#ltcommentsgtSubelementOfLtitemgt).
	//
	// Sample:
	//   http://www.myblog.org/cgi-local/mt/mt-comments.cgi?entry_id=290
	Comments string `xml:"comments,omitempty" json:"comments,omitempty"`

	// Describes a media object that is attached to the item.
	// [More](https://cyber.harvard.edu/rss/rss.html#ltenclosuregtSubelementOfLtitemgt).
	Enclosure *RSSEnclosure `xml:"enclosure,omitempty" json:"enclosure,omitempty"`

	// A string that uniquely identifies the item.
	// [More](https://cyber.harvard.edu/rss/rss.html#ltguidgtSubelementOfLtitemgt).
	//
	// Sample:
	//   http://inessential.com/2002/09/01.php#a2
	GUID *RSSGUID `xml:"guid,omitempty" json:"guid,omitempty"`

	// Indicates when the item was published.
	// [More](https://cyber.harvard.edu/rss/rss.html#ltpubdategtSubelementOfLtitemgt).
	//
	// Sample:
	//   Sun, 19 May 2002 15:21:36 GMT
	PubDate *RFC822 `xml:"pubDate,omitempty" json:"pub_date,omitempty"`

	// The RSS channel that the item came from.
	// [More](https://cyber.harvard.edu/rss/rss.html#ltsourcegtSubelementOfLtitemgt).
	//
	// Sample:
	//   <source url="http://www.tomalak.org/links2.xml">Tomalak's Realm</source>
	Source *RSSSource `xml:"source,omitempty" json:"source,omitempty"`
}

func (it RSSItem) String() string {
	return fmt.Sprintf(
		"RSSItem{Title:%q, Link:%q, Description:%q, Author:%q, Categories:%v, "+
			"Comments:%q, Enclosure:%v, GUID:%v, PubDate:%v, Source:%v}",
		it.Title, it.Link, it.Description, it.Author, it.Categories,
		it.Comments, it.Enclosure, it.GUID, it.PubDate, it.Source,
	)
}

func (it RSSItem) Equal(t RSSItem) bool {
	if it.Categories != nil && t.Categories != nil &&
		!it.Categories.Equal(t.Categories) {
		return false
	}
	if it.Enclosure != nil && t.Enclosure != nil &&
		!it.Enclosure.Equal(*t.Enclosure) {
		return false
	}
	if it.GUID != nil && t.GUID != nil && !it.GUID.Equal(*t.GUID) {
		return false
	}
	if it.PubDate != nil && t.PubDate != nil && !it.PubDate.Equal(*t.PubDate) {
		return false
	}
	if it.Source != nil && t.Source != nil && !it.Source.Equal(*t.Source) {
		return false
	}

	return it.Title == t.Title && it.Link == t.Link &&
		it.Description == t.Description && it.Author == t.Author &&
		it.Comments == t.Comments
}

func (it RSSItem) MD5() string {
	return fmt.Sprintf("%s", md5.Sum([]byte(it.String())))
}

type RSSGUID struct {
	GUID        string `xml:",chardata" json:"guid"`
	IsPermaLink bool   `xml:"isPermaLink,attr" json:"is_perma_link"`
}

func (g RSSGUID) String() string {
	return fmt.Sprintf("RSSGUID{GUID:%q, IsPermaLink:%v}", g.GUID, g.IsPermaLink)
}

func (g RSSGUID) Equal(t RSSGUID) bool {
	return g.GUID == t.GUID && g.IsPermaLink == t.IsPermaLink
}

// RSSEnclosure is an optional sub-element of RSSItem.
//
// It has three required attributes. url says where the enclosure is
// located, length says how big it is in bytes, and type says what its
// type is, a standard MIME type.
//
// The url must be an http url.
//
// <enclosure url="http://www.scripting.com/mp3s/weatherReportSuite.mp3" length="12216320" type="audio/mpeg" />
//
// A use-case narrative for this element is [here](http://www.thetwowayweb.com/payloadsforrss).
type RSSEnclosure struct {

	/*************************** Required elements ***************************/

	URL    string `xml:"url,attr"    json:"url"`
	Length int    `xml:"length,attr" json:"length"`
	Type   string `xml:"type,attr"   json:"type"`

	/*************************** Optional elements ***************************/

	// No optional element.
}

func (ec RSSEnclosure) String() string {
	return fmt.Sprintf(
		"RSSEnclosure{URL:%q, Length:%d, Type:%q}",
		ec.URL, ec.Length, ec.Type,
	)
}

func (ec RSSEnclosure) Equal(t RSSEnclosure) bool {
	return ec.URL == t.URL && ec.Length == t.Length && ec.Type == t.Type
}

// RSSSource is an optional sub-element of RSSItem.
//
// Its value is the name of the RSSChannel that the item came from,
// derived from its <title>. It has one required attribute, url, which
// links to the XMLization of the source.
//
// <source url="http://www.tomalak.org/links2.xml">Tomalak's Realm</source>
//
// The purpose of this element is to propagate credit for links, to
// publicize the sources of news items. It can be used in the Post
// command of an aggregator. It should be generated automatically when
// forwarding an item from an aggregator to a weblog authoring tool.
type RSSSource struct {

	/*************************** Required elements ***************************/

	Source string `xml:",chardata" json:"source"`
	URL    string `xml:"url,attr" json:"url"`

	/*************************** Optional elements ***************************/

	// No optional element.
}

func (s RSSSource) String() string {
	return fmt.Sprintf("RSSSource{Source:%q, URL:%q}", s.Source, s.URL)
}

func (s RSSSource) Equal(t RSSSource) bool {
	return s.Source == t.Source && s.URL == t.URL
}

type RFC822 time.Time

var rfc822layout = [2]string{
	"Mon, 02 Jan 2006 15:04:05 MST",
	"Mon, 02 Jan 2006 15:04:05 -0700",
}

func (r RFC822) String() string { return time.Time(r).Format(time.RFC3339) }

// UnmarshalXML implements the xml.Unmarshal interface.
func (r *RFC822) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var v, layout string
	var t time.Time
	var err error
	d.DecodeElement(&v, &start)
	for _, layout = range rfc822layout {
		t, err = time.Parse(layout, v)
		if err == nil {
			*r = RFC822(t)
			return nil
		}
	}
	return err
}

// MarshalJSON implements the json.Marshal interface.
func (r *RFC822) MarshalJSON() ([]byte, error) {
	return json.Marshal(r.String())
}

// IsZero reports whether r represents the zero time instant,
// January 1, year 1, 00:00:00 UTC.
func (r RFC822) IsZero() bool { return time.Time(r).IsZero() }

// After reports whether the RFC822 instant r is after t.
func (r RFC822) After(t RFC822) bool { return time.Time(r).After(time.Time(t)) }

func (r RFC822) Equal(t RFC822) bool {
	return time.Time(r).UTC() == time.Time(t).UTC()
}

func (r RFC822) Format(layout string) string {
	return time.Time(r).Format(layout)
}

// vim: ts=4 sts=4 sw=4 noet
