# vim: ts=4 sts=4 sw=4 et

# Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
# All rights reserved.
# Use of this source code is governed by a GPLv3
# license that can be found in the LICENSE file.

# USAGE:
#
# - Build Docker Image:
#     docker build -t coybot:latest .
#
# - Run Docker Container:
#     docker run --name coybot --rm -t coybot:latest

FROM golang:alpine AS builder
ENV CGO_ENABLED=0  # docker scratch need this option
RUN set -ex \
    && apk add git \
    && go get \
        -v \
        -fix \
        -ldflags='-s -w' \
        gitlab.com/coybot/coybot

FROM scratch
COPY --from=builder /go/bin/coybot /coybot
ENTRYPOINT ["/coybot"]
