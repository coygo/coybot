// vim: ts=4 sts=4 sw=4 noet

// Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
// All rights reserved.
// Use of this source code is governed by a GPLv3
// license that can be found in the LICENSE file.

package matrixapi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"
)

type Matrix struct {
	HomeServer  string
	User        string
	Password    string
	AccessToken string
	DeviceID    string
}

// Login authenticates the user, and issues an access token they can use to authorize themself in subsequent requests.
func (m Matrix) Login() error {
	if m.User == "" || m.Password == "" {
		return fmt.Errorf("empty User or Password")
	}

	type LoginRequest struct {
		// Required
		// The login type being used. One of: ["m.login.password", "m.login.token"]
		Type string `json:"type"`

		// The fully qualified user ID or just local part of the user ID, to log in.
		User string `json:"user,omitempty"`

		// When logging in using a third party identifier, the medium of the
		// identifier. Must be 'email'.
		Medium string `json:"medium,omitempty"`

		// Third party identifier for the user.
		Address string `json:"address,omitempty"`

		// Required when type is m.login.password. The user's password.
		Password string `json:"password,omitempty"`

		// Required when type is m.login.token. The login token.
		Token string `json:"token,omitempty"`

		// ID of the client device. If this does not correspond to a known client device, a new device will be created. The server will auto-generate a device_id if this is not specified.
		DeviceID string `json:"device_id,omitempty"`

		// A display name to assign to the newly-created device. Ignored if DeviceID corresponds to a known device.
		InitialDeviceDisplayName string `json:"initial_device_display_name,omitempty"`
	}
	type LoginResponse struct {
		// The fully-qualified Matrix ID that has been registered.
		UserID string `json:"user_id"`

		// An access token for the account. This access token can then be used to authorize other requests.
		AccessToken string `json:"access_token"`

		// The hostname of the homeserver on which the account has been registered.
		HomeServer string `json:"home_server"`

		// ID of the logged-in device. Will be the same as the corresponding parameter in the request, if one was specified.
		DeviceID string `json:"device_id"`
	}

	loginRequest := LoginRequest{
		Type:                     "m.login.password",
		User:                     m.User,
		Password:                 m.Password,
		InitialDeviceDisplayName: "Coy Bot in Go",
	}

	uri := "/_matrix/client/r0/login"

	r, err := restPost(m.HomeServer, uri, "", loginRequest)
	if err != nil {
		return err
	}

	if r.StatusCode != http.StatusOK {
		return fmt.Errorf("getting %q: %q, %q", uri, r.Status, r.Content)
	}

	var loginResponse LoginResponse
	err = json.Unmarshal(r.Content, &loginResponse)
	if err != nil {
		return err
	}

	m.HomeServer = loginResponse.HomeServer
	m.AccessToken = loginResponse.AccessToken
	m.User = loginResponse.UserID
	m.DeviceID = loginResponse.DeviceID

	return nil
}

// Logout invalidates an existing access token, so that it can no longer be used for authorization.
func (m Matrix) Logout() error {
	if m.AccessToken == "" {
		return fmt.Errorf("empty AccessToken")
	}

	uri := "/_matrix/client/r0/logout"

	// NOTE: The required AccessToken is sent by rest() automatically.
	r, err := restPost(m.HomeServer, uri, m.AccessToken, nil)
	if err != nil {
		return err
	}

	if r.StatusCode != http.StatusOK {
		return fmt.Errorf("getting %q: %q, %q", uri, r.Status, r.Content)
	}

	m.AccessToken = ""

	return nil
}

// SendRoomMessage sends message event to a room.
func (m Matrix) SendRoomMessage(roomID string, message interface{}) (eventID string, err error) {
	switch message.(type) {
	case *RoomMessageText, *RoomMessageEmote, *RoomMessageNotice:
		// Do nothing.
	default:
		return "", fmt.Errorf("wrong type message: %T", message)
	}

	type JSONResponse struct {
		EventID string `json:"event_id"`
	}

	tnxID := time.Now().UnixNano()
	uri := fmt.Sprintf("/_matrix/client/r0/rooms/%s/send/m.room.message/%d", roomID, tnxID)
	var jsonResponse JSONResponse

	st, err := restPut(m.HomeServer, uri, m.AccessToken, message)
	if err != nil {
		return "", err
	}

	_ = json.Unmarshal(st.Content, &jsonResponse) // NOTE: error is ignored

	if st.StatusCode != http.StatusOK {
		return "", fmt.Errorf("getting %q: %q, %#v", uri, st.Status, string(st.Content))
	}

	return jsonResponse.EventID, nil
}

type RoomMessageText struct {

	/******* REQUIRED FIELDS *******/

	// Must be "m.text".
	Type string `json:"msgtype"`

	// The body of the message.
	Body string `json:"body"`

	/******* OPTIONAL FIELDS *******/

	// The format used in the FormattedBody.
	// Currently only "org.matrix.custom.html" is supported.
	Format string `json:"format,omitempty"`

	// The formatted version of the Body.
	// This is required if Format is specified.
	FormattedBody string `json:"formatted_body,omitempty"`
}

func NewRoomMessageText(body, formattedBody string) *RoomMessageText {
	msg := &RoomMessageText{Type: "m.text", Body: body}

	if formattedBody != "" {
		msg.Format = "org.matrix.custom.html"
		msg.FormattedBody = formattedBody
	}

	return msg
}

type RoomMessageEmote struct {

	/******* REQUIRED FIELDS *******/

	// Must be "m.emote".
	Type string `json:"msgtype"`

	// The emote action to perform.
	Body string `json:"body"`

	/******* OPTIONAL FIELDS *******/

	// The format used in the FormattedBody.
	// Currently only "org.matrix.custom.html" is supported.
	Format string `json:"format,omitempty"`

	// The formatted version of the Body.
	// This is required if Format is specified.
	FormattedBody string `json:"formatted_body,omitempty"`
}

func NewRoomMessageEmote(body, formattedBody string) *RoomMessageEmote {
	msg := &RoomMessageEmote{Type: "m.emote", Body: body}

	if formattedBody != "" {
		msg.Format = "org.matrix.custom.html"
		msg.FormattedBody = formattedBody
	}

	return msg
}

type RoomMessageNotice struct {

	/******* REQUIRED FIELDS *******/

	// Must be "m.notice".
	Type string `json:"msgtype"`

	// The notice text to send.
	Body string `json:"body"`

	/******* OPTIONAL FIELDS *******/

	// The format used in the FormattedBody.
	// Currently only "org.matrix.custom.html" is supported.
	Format string `json:"format,omitempty"`

	// The formatted version of the Body.
	// This is required if Format is specified.
	FormattedBody string `json:"formatted_body,omitempty"`
}

func NewRoomMessageNotice(body, formattedBody string) *RoomMessageNotice {
	msg := &RoomMessageNotice{Type: "m.notice", Body: body}

	if formattedBody != "" {
		msg.Format = "org.matrix.custom.html"
		msg.FormattedBody = formattedBody
	}

	return msg
}

type RoomMessageImage struct {

	/******* REQUIRED FIELDS *******/

	// Must be "m.image".
	Type string `json:"msgtype"`

	// A textual representation of the image. This could be the alt text of the
	// image, the filename of the image, or some kind of content description for
	// accessibility e.g. "image attachment".
	Body string `json:"body"`

	// The URL to the image.
	URL url.URL `json:"url"`

	/******* OPTIONAL FIELDS *******/

	// Metadata about the image referred to in url.
	Info ImageInfo `json:"info,omitempty"`
}

type RoomMessageFile struct {

	/******* REQUIRED FIELDS *******/

	// Must be "m.file".
	Type string `json:"m.msgtype"`

	// A human-readable description of the file. This is recommended to be the
	// filename of the original upload.
	Body string `json:"body"`

	// The original filename of the uploaded file.
	Filename string `json:"filename"`

	// The URL to the file.
	URL url.URL `json:"url"`

	/******* OPTIONAL FIELDS *******/

	// Information about the file referred to in URL.
	Info FileInfo `json:"info,omitempty"`
}

type RoomMessageAudio struct {

	/******* REQUIRED FIELDS *******/

	// Must be "m.audio".
	Type string `json:"msgtype"`

	// A description of the audio e.g. "Bee Gees - Stayin" Alive", or some kind
	// of content description for accessibility e.g. "audio attachment".
	Body string `json:"body"`

	// The URL to the audio clip.
	URL url.URL `json:"url"`

	/******* OPTIONAL FIELDS *******/

	// Metadata for the audio clip referred to in URL.
	Info AudioInfo `json:"info,omitempty"`
}

type RoomMessageVideo struct {

	/******* REQUIRED FIELDS *******/

	// Must be "m.video".
	Type string `json:"msgtype"`

	// A description of the video e.g. "Gangnam style", or some kind of content
	// description for accessibility e.g. "video attachment".
	Body string `json:"body"`

	// The URL to the video clip.
	URL url.URL `json:"url"`

	/******* OPTIONAL FIELDS *******/

	// Metadata about the video clip referred to in URL.
	Info VideoInfo `json:"info,omitempty"`
}

type RoomMessageLocation struct {

	/******* REQUIRED FIELDS *******/

	// Must be "m.location".
	Type string `json:"msgtype"`

	// A description of the location e.g. "Big Ben, London, UK", or some kind of
	// content description for accessibility e.g. "location attachment".
	Body string `json:"body"`

	// A geo URI representing this location.
	GeoURI string `json:"geo_uri"`

	/******* OPTIONAL FIELDS *******/

	Info LocationInfo `json:"info,omitempty"`
}

type ImageInfo struct {
	// The intended display height of the image in pixels.
	// This may differ from the intrinsic dimensions of the image file.
	Height int `json:"h"`

	// The intended display width of the image in pixels.
	// This may differ from the intrinsic dimensions of the image file.
	Width int `json:"w"`

	// The mimetype of the image, e.g. "image/jpeg".
	MimeType string `json:"mimetype"`

	// Size of the image in bytes.
	Size int `json:"size"`

	// The URL to a thumbnail of the image.
	ThumbnailURL url.URL `json:"thumbnail_url"`

	// Metadata about the image referred to in ThumbnailURL.
	ThumbnailInfo ThumbnailInfo `json:"thumbnail_info"`
}

type ThumbnailInfo struct {
	// The intended display height of the image in pixels.
	// This may differ from the intrinsic dimensions of the image file.
	Height int `json:"h"`

	// The intended display width of the image in pixels.
	// This may differ from the intrinsic dimensions of the image file.
	Width int `json:"w"`

	// The mimetype of the image, e.g. "image/jpeg".
	MIMEType string `json:"mimetype"`

	// Size of the image in bytes.
	Size int `json:"size"`
}

type FileInfo struct {
	// The mimetype of the file e.g. "application/msword".
	MIMEType string `json:"mimetype"`

	// The size of the file in bytes.
	Size int `json:"size"`

	// The URL to the thumbnail of the file.
	ThumbnailURL url.URL `json:"thumbnail_url"`

	// Metadata about the image referred to in ThumbnailURL.
	ThumbnailInfo ThumbnailInfo `json:"thumbnail_info"`
}

type AudioInfo struct {
	// The duration of the audio in milliseconds.
	Duration int `json:"duration"`

	// The mimetype of the audio e.g. audio/aac.
	MIMEType string `json:"mimetype"`

	// The size of the audio clip in bytes.
	Size int `json:"size"`
}

type VideoInfo struct {
	// The duration of the video in milliseconds.
	Duration int `json:"duration"`

	// The height of the video in pixels.
	Height int `json:"h"`

	// The width of the video in pixels.
	Width int `json:"w"`

	// The mimetype of the video e.g. "video/mp4".
	MIMEType string `json:"mimetype"`

	// The size of the video in bytes.
	Size int `json:"size"`

	// The URL to an image thumbnail of the video clip.
	ThumbnailURL url.URL `json:"thumbnail_url"`

	// Metadata about the image referred to in ThumbnailURL.
	ThumbnailInfo ThumbnailInfo `json:"thumbnail_info"`
}

type LocationInfo struct {
	// The URL to a thumbnail of the location being represented.
	ThumbnailURL string `json:"thumbnail_url"`

	// Metadata about the image referred to in ThumbnailURL.
	ThumbnailInfo ThumbnailInfo `json:"thumbnail_info"`
}
