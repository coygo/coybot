#!/bin/sh
# vim: ts=4 sts=4 sw=4 et
#
# Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
# All rights reserved.
# Use of this source code is governed by a GPLv3
# license that can be found in the LICENSE file.

set -e

CWD="$(dirname $(realpath $0))"

. ${CWD}/init_colors.inc.sh

check_gpgsign() {
    # "|| true" means force success
    no_gpg_sign=$(git config --get hooks.noGPGSign || true)
    gpg_signing_key=$(git config --get user.signingKey || true)
    commit_gpg_sign=$(git config --get commit.gpgSign || true)

    if ([ "$commit_gpg_sign" != "true" ] || [ -z $gpg_signing_key ]) && [ "$no_gpg_sign" != "true" ]
    then
        echo
        echo ${RED} ERROR ${RESET}
        echo
        echo You must sign the commit with a GPG Key. You can set your git repo to sign automatically by:
        echo
        echo "    git config commit.gpgSign true"
        echo "    git config user.signingKey <your-signing-key>"
        echo
        echo See https://gitlab.com/help/user/project/repository/gpg_signed_commits/index.md
        echo
        echo If you know what you are doing you can disable this check using:
        echo
        echo "    git config hooks.noGPGSign true"
        echo

        exit 1
    fi
}

check_gpgsign
