#!/bin/bash
# vim: ts=4 sts=4 sw=4 et
#
# Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
# All rights reserved.
# Use of this source code is governed by a GPLv3
# license that can be found in the LICENSE file.

set -e

CWD="$(dirname $(realpath $0))"

. ${CWD}/init_colors.inc.sh

echo ${YELLOW}Checking Go Version...${RESET}
go version

echo ${YELLOW}Linting...${RESET}
# golint -set_exit_status
golint
echo ${GREEN}Lint OK${RESET}

echo ${YELLOW}Checking Spell...${RESET}
misspell -error $(find . -type f ! -path ./vendor/\* ! -path ./.git/\*)
echo ${GREEN}Spell OK${RESET}

echo ${YELLOW}Checking Code Format...${RESET}
# gofmt doesn't exit with an error code if the files don't match the expected
# format. So we have to run it and see if it outputs anything.
if gofmt -l -s $(find . -name \*.go -type f ! -path ./vendor/\* ! -path ./.git/\*) 2>&1 | read
then
    echo
    echo "  "${REVERSE}${RED}Error: not all code had been formatted with gofmt.${RESET}
    echo
    echo "  "${GREEN}Fixing the following files${RESET}
    gofmt -s -w -l $(find . -name \*.go -type f ! -path ./vendor/\* ! -path ./.git/\*) | sed "s/^/    /g"
    echo
    echo "  "${GREEN}Please add them to the commit${RESET}
    git status --short | sed "s/^/    /g"

    exit 1
fi
echo ${GREEN}Code Format OK${RESET}

echo ${YELLOW}Detect Ineffectual Assignments...${RESET}
ineffassign .
echo ${GREEN}Ineffectual Assignments OK${RESET}

echo ${YELLOW}Formating...${RESET}
go fmt

echo ${YELLOW}Calculating Cyclomatic Complexities of Functions...${RESET}
gocyclo -over 12 $(find -L *.go ! -name '*_test.go')
echo ${GREEN}Cyclomatic OK${RESET}

echo ${YELLOW}Testing...${RESET}
go test -timeout 10s -v
echo ${GREEN}Test OK${RESET}
