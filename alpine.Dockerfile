# vim: ts=4 sts=4 sw=4 et

# Copyright 2018, 2019 Coy <https://gitlab.com/coygo>.
# All rights reserved.
# Use of this source code is governed by a GPLv3
# license that can be found in the LICENSE file.

# USAGE:
#
# - Build Docker Image:
#     docker build -f alpine.Dockerfile -t coybot:alpine .
#
# - Run Docker Container:
#     docker run --name coybot --rm -t coybot:alpine

FROM golang:alpine AS builder
RUN set -ex \
    && apk add git \
    && go get \
        -v \
        -fix \
        -ldflags='-s -w' \
        gitlab.com/coybot/coybot

FROM alpine
RUN apk add --no-cache ca-certificates
COPY --from=builder /go/bin/coybot /coybot
ENTRYPOINT ["/coybot"]
