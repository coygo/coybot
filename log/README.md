# gitlab.com/coygo/log

An alternative Go logger.

Package log wraps Go's log package and provides a simple level-able and color-able logging package. It has 5 levels of logging in order from highest to lowest as listed below.

Error > Warning > Info > Debug > Trace

Package log is licensed in GPLv3.

## Example

```go
package main

import "gitlab.com/coygo/log"

func main() {
	log.SetFlags(log.LstdFlags | log.Ltrace)

	log.Trace("A trace level logging event in color magenta.")
	log.Debugf("A debug level one. %s", "It works like fmt.Printf(), but colored in blue.")
	log.Infoln("A info level one works like fmt.Println(), but colored in green.")
	log.Warning("A yellow one.")
	log.Error("A error level event should be in red.")
	log.Print("No color logging event. It is a log.Print() from Go official log package in fact.")
	log.Fatal("A Go official's log.Fatal() indeed. In fact, the program will stop here.")
	log.Panic("This line will never reached.")
}
```

## Install

```bash
go get -u gitlab.com/coygo/log
```
